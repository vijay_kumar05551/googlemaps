//
//  ViewController.h
//  lack project
//
//  Created by Click Labs135 on 10/26/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController : UIViewController<CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;


@end

