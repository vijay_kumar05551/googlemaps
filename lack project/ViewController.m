//
//  ViewController.m
//  lack project
//
//  Created by Click Labs135 on 10/26/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
NSMutableArray *mpArray;

@interface ViewController (){
    CLLocationManager *manager;
}


@property (strong, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (strong, nonatomic) IBOutlet UITextField *latitudeTextField;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (strong, nonatomic) IBOutlet UITextField *longitudeTextField;
@property (strong, nonatomic) IBOutlet UIButton *getLocation;


@end

@implementation ViewController
@synthesize latitudeLabel;
@synthesize latitudeTextField;
@synthesize longitudeLabel;
@synthesize longitudeTextField;
@synthesize getLocation;
GMSMapView *mapView;

- (void)viewDidLoad {

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    manager=[[CLLocationManager alloc]init];
    manager.delegate=self;
    manager.desiredAccuracy=kCLLocationAccuracyBest;
    [manager requestAlwaysAuthorization];
    
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:6];
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.myLocationEnabled = YES;
    self.view = mapView;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = mapView;
  
    
    }
- (IBAction)getLocationPressed:(id)sender {
    [manager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        longitudeTextField.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitudeTextField.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
